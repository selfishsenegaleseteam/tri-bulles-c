﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        //Jeu de données
        public static List<int> mesInt = new List<int>();
        public static List<char> mesChars = new List<char>();
        public static List<string> mesString = new List<string>();
        public static List<double> mesDouble = new List<double>();
        public static List<bool> mesBoules = new List<bool>();
        public static List<Etudiant> mesEtudiants = new List<Etudiant>();

        //utilitaire de reflexion
        static BindingFlags FLAGS = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;
        /*
         * La methode Tri Bulle customisee agit comme un orderBy de LINQ
         * On lui fournit un argument optionnel(l'attribut discriminant)
         * ainsi qu'une liste parametree a trier
        */
        public static void TriBulle<T>(List<T> monTableau, String champ = null)
        {

            
            //On prend le type
            Type type = typeof(T);
            //Grace au mécanisme d'introspection, l'on se renseigne sur le champ
            //On détecte d'abord si le type du tableau est un typeValeur
            //C'est un cas simple à traiter
            bool typeValeur = typeof(T).IsValueType;
            //Si ce n'est pas de type valeur, on cherche si le membre à comparer est un string 
            bool attributDeTypeValeur = false;
            if (!typeValeur) {
                //en faire un ternaire
                attributDeTypeValeur = (typeof(T).ToString().Contains("String"));
            }
            bool permutation = true;
            do
            {
                permutation = false;
                for (int i = 0; i < 5; i++)
                {

                    if (typeValeur)
                    {
                        //Boxing pour duper le compilateur
                        object o1 = monTableau[i];
                        object o2 = monTableau[i + 1];
                        if ((typeof(T).Equals("System.Double")) || (typeof(T).Equals("System.Float")))
                        {
                            //unboxing dans un type supérieur (double en l'occurrence)
                            double d1 = Convert.ToDouble(o1);
                            double d2 = Convert.ToDouble(o2);
                            if (d1 > d2)
                            {
                                //On permute les cases i et i+1
                                T temp = monTableau[i];
                                monTableau[i] = monTableau[i + 1];
                                monTableau[i + 1] = temp;
                                permutation = true;
                            }
                        }
                        else
                        {
                            //unboxing dans un type supérieur (int en l'occurrence)
                            int d1 = Convert.ToInt32(o1);
                            int d2 = Convert.ToInt32(o2);
                            if (d1 > d2)
                            {
                                //On permute les cases i et i+1
                                T temp = monTableau[i];
                                monTableau[i] = monTableau[i + 1];
                                monTableau[i + 1] = temp;
                                permutation = true;
                            }
                        }

                    }
                    else {
                        
                        //Si c'est un string
                        if (attributDeTypeValeur)
                        {
                            //C'est de loin plus aisé...
                            object s1 = monTableau[i];
                            object s2 = monTableau[i + 1];
                            int val = String.CompareOrdinal((String)s1, (String)s2);
                            if (val > 0)
                            {
                                //On permute les cases i et i+1
                                T temp = monTableau[i];
                                monTableau[i] = monTableau[i + 1];
                                monTableau[i + 1] = temp;
                                permutation = true;
                            }
                        }
                        else {
                            Type myType = typeof(T);
                            PropertyInfo myPropInfo = myType.GetProperty(champ);
                            Type typeAttribut = GetField(typeof(T), champ, monTableau[0], FLAGS).GetType();
                            
                            if ((typeAttribut.ToString().Contains("Double")) || (typeAttribut.ToString().Contains("Float")))
                            {
                                
                                double a1 = (double)GetField(typeof(T), champ, monTableau[i], FLAGS);
                                double a2 = (double)GetField(typeof(T), champ, monTableau[i+1], FLAGS);
                                if (a1 > a2)
                                {
                                    T temp = monTableau[i];
                                    monTableau[i] = monTableau[i + 1];
                                    monTableau[i + 1] = temp;
                                    permutation = true;
                                }
                            }
                            else if (typeAttribut.ToString().Contains("String"))
                            {
                                String a1 = (String)GetField(typeof(T), champ, monTableau[i], FLAGS);
                                String a2 = (String)GetField(typeof(T), champ, monTableau[i+1], FLAGS);
                                int val = String.CompareOrdinal(a1, a2);
                                if (val > 0)
                                {
                                    //On permute les cases i et i+1
                                    T temp = monTableau[i];
                                    monTableau[i] = monTableau[i + 1];
                                    monTableau[i + 1] = temp;
                                    permutation = true;
                                }
                            }
                            else if (typeAttribut.IsClass)
                            {
                                Console.WriteLine("Le critère de tri ne peut être un objet");
                            }

                            else
                            {
                                int a1 = (int)GetField(typeof(T), champ, monTableau[i], FLAGS);
                                int a2 = (int)GetField(typeof(T), champ, monTableau[i+1], FLAGS);
                                
                                if (a1 > a2)
                                {
                                    //On permute les cases i et i+1
                                    T temp = monTableau[i];
                                    monTableau[i] = monTableau[i + 1];
                                    monTableau[i + 1] = temp;
                                    permutation = true;
                                }
                            }


                        }
                    }
                }
            } while (permutation);
        }

        //Methode utilitaire permettant de réaliser l'introspection
        private static object GetField(System.Type t, string strField, object objInstance, System.Reflection.BindingFlags eFlags)
        {
            System.Reflection.FieldInfo f;
            try
            {
                f = t.GetField(strField, eFlags);

                if (f == null)
                {
                    throw new ArgumentException("There is no field '" + strField + "' for type '" + t.ToString() + "'.");
                }

                object objRet = f.GetValue(objInstance); ;
                return objRet;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static void loadData() {
            //On crée une liste d'entiers
            mesInt.Add(25);
            mesInt.Add(10);
            mesInt.Add(13);
            mesInt.Add(9);
            mesInt.Add(26);
            mesInt.Add(7);

            //On crée une liste de chars
            mesChars.Add('a');
            mesChars.Add('z');
            mesChars.Add('b');
            mesChars.Add('w');
            mesChars.Add('u');
            mesChars.Add('m');

            //on crée une liste de string
            mesString.Add("Birane");
            mesString.Add("Joachim");
            mesString.Add("Didier");
            mesString.Add("Assane");
            mesString.Add("Ulrich");
            mesString.Add("Mamadou");

            //on crée une liste de double
            mesDouble.Add(15.785);
            mesDouble.Add(27.987);
            mesDouble.Add(17.784);
            mesDouble.Add(22.52);
            mesDouble.Add(-13.0);
            mesDouble.Add(78);

            //On crée une liste de booléens
            mesBoules.Add(true);
            mesBoules.Add(false);
            mesBoules.Add(true);
            mesBoules.Add(false);
            mesBoules.Add(false);
            mesBoules.Add(true);

            //On crée une liste d'étudiants
            mesEtudiants.Add(new Etudiant("Mame", 17.23, "201684xbf"));
            mesEtudiants.Add(new Etudiant("Djibi", 14.78, "201678962"));
            mesEtudiants.Add(new Etudiant("Pierre", 18.00, "201305d22"));
            mesEtudiants.Add(new Etudiant("Trixy", 09.63, "025652025"));
            mesEtudiants.Add(new Etudiant("Rosalina", 17.17, "7554654"));
            mesEtudiants.Add(new Etudiant("Mamadou", 19.00, "2012051EM"));
        }

        public static void sort() {
            //Tri d'entiers
            Console.WriteLine("***Tri d'entiers:***");
            Console.WriteLine("***Avant Tri:***");
            show(mesInt);
            TriBulle<int>(mesInt);
            Console.WriteLine("***Après Tri:***");
            show(mesInt);

            //Tri de chars
            Console.Clear();
            Console.WriteLine("***Tri de chars:***");
            Console.WriteLine("***Avant Tri:***");
            show(mesChars);
            TriBulle<char>(mesChars);
            Console.WriteLine("***Après Tri:***");
            show(mesChars);

            //Tri de Doubles
            Console.Clear();
            Console.WriteLine("***Tri de Double:***");
            Console.WriteLine("***Avant Tri:***");
            show(mesDouble);
            TriBulle<double>(mesDouble);
            Console.WriteLine("***Après Tri:***");
            show(mesDouble);

            //Tri de Strings
            Console.Clear();
            Console.WriteLine("***Tri de String:***");
            Console.WriteLine("***Avant Tri:***");
            show(mesString);
            TriBulle<string>(mesString);
            Console.WriteLine("***Après Tri:***");
            show(mesString);

            //Tri de booléens
            Console.Clear();
            Console.WriteLine("***Tri de Booleens:***");
            Console.WriteLine("***Avant Tri:***");
            show(mesBoules);
            TriBulle<bool>(mesBoules);
            Console.WriteLine("***Après Tri:***");
            show(mesBoules);

            //Tri d'Etudiants avec critère moyenne
            Console.Clear();
            Console.WriteLine("***Tri d'etudiants:***");
            Console.WriteLine("***Avant Tri:***");
            show(mesEtudiants);
            TriBulle<Etudiant>(mesEtudiants, "moyenne");
            Console.WriteLine("***Après Tri:***");
            show(mesEtudiants);

            //Tri d'Etudiants avec critère nom
            Console.Clear();
            Console.WriteLine("***Tri d'etudiants:***");
            Console.WriteLine("***Avant Tri:***");
            show(mesEtudiants);
            TriBulle<Etudiant>(mesEtudiants, "nom");
            Console.WriteLine("***Après Tri:***");
            show(mesEtudiants);
        }
        static void Main(string[] args)
        {
            loadData();
            sort();
        }
        public static void show<T>(List<T> monTab)
        {
            for (int i = 0; i <= 5; i++)
            {
                Console.WriteLine(monTab[i]);
            }
            Console.WriteLine("Appuyez sur une touche pour trier");
            Console.ReadKey();
        }
        public static void showEt<Etudiant>(List<Etudiant> monTab)
        {
            for (int i = 0; i <= 5; i++)
            {
                Console.WriteLine(monTab[i].ToString());
            }
            Console.WriteLine("Appuyez sur une touche pour trier");
            Console.ReadKey();
        }

    }
}
