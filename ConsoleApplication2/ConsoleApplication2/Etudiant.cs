﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Etudiant
    {
        private String nom;
        private double moyenne;
        private String nCE;

        public Etudiant(String nom, double moyenne, string nCE) {
            this.nom = nom;
            this.moyenne = moyenne;
            this.nCE = nCE;
        }
        public override String ToString() {
            return "nom: " + nom + "\nmoyenne: " + moyenne + "\n numero: " + nCE;
        }
    }
}
